//
//  HistoricalPriceDetailPresenter.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

internal protocol DetailView: class {
    var title: String? { get set }
    func show(date: String)
    func showPrice(euros: String?, dollars: String?, pounds: String?)
}

internal protocol DetailPresenterProtocol: class {

    var view: DetailView? { get set }
    func loadView()
}

internal final class HistoricalPriceDetailPresenter: DetailPresenterProtocol {

    // MARK: - Properties
    private let historicalPrice: Price

    // MARK: - Variables
    weak var view: DetailView?

    init(historicalPrice: Price) {
        self.historicalPrice = historicalPrice
    }

    // MARK: - DetailPresenterProtocol
    func loadView() {

        view?.title = "core_detail_title".localized
        fetchDate()
        fetchPrice()
    }

    // MARK: - Private Methods
    private func fetchDate() {

        let date = historicalPrice.date
        let formattedDate = DateManager.string(from: date, format: .ddMMyyyy)
        view?.show(date: formattedDate)
    }

    private func fetchPrice() {

        let euros = historicalPrice.value
        let eurosFormatted = CurrencyManager.format(value: euros)

        var dollarsFormatted: String?
        if let dollars = CurrencyManager.convertToDollars(euros: euros) {
            dollarsFormatted = CurrencyManager.format(value: dollars, currency: .dollar)
        }

        var poundsFormatted: String?
        if let pounds = CurrencyManager.convertToPounds(euros: euros) {
            poundsFormatted = CurrencyManager.format(value: pounds, currency: .pound)
        }

        view?.showPrice(euros: eurosFormatted,
                        dollars: dollarsFormatted,
                        pounds: poundsFormatted)
    }
}
