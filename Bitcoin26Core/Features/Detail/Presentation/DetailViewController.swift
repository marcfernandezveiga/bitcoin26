//
//  DetailViewController.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

protocol DetailViewControllerProvider: class {
    func detailViewController<T>(item: T) -> UIViewController?
}

internal final class DetailViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var eurosLabel: UILabel!
    @IBOutlet private weak var dollarsLabel: UILabel!
    @IBOutlet private weak var poundsLabel: UILabel!

    // MARK: - Properties
    private let presenter: DetailPresenterProtocol

    // MARK: - Initializers
    init(presenter: DetailPresenterProtocol) {
        self.presenter = presenter

        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.view = self

        presenter.loadView()
    }
}

// MARK: - DetailView
extension DetailViewController: DetailView {

    func show(date: String) {

        dateLabel.text = date
    }

    func showPrice(euros: String?, dollars: String?, pounds: String?) {

        eurosLabel.text = euros
        dollarsLabel.text = dollars
        poundsLabel.text = pounds
    }
}
