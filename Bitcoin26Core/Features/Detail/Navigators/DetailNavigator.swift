//
//  DetailNavigator.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

internal protocol DetailNavigator {
    func showDetail<T>(item: T)
}
