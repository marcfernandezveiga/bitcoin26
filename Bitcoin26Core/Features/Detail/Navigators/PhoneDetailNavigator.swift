//
//  PhoneDetailNavigator.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

internal final class PhoneDetailNavigator: DetailNavigator {

    // MARK: - Properties
    private let navigationController: UINavigationController
    private unowned let viewControllerProvider: DetailViewControllerProvider

    init(navigationController: UINavigationController, viewControllerProvider: DetailViewControllerProvider) {
        self.navigationController = navigationController
        self.viewControllerProvider = viewControllerProvider
    }

    // MARK: - DetailNavigator
    func showDetail<T>(item: T) {

        if let viewController = viewControllerProvider.detailViewController(item: item) {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
}
