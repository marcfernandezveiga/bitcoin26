//
//  DetailAssembly.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

internal final class DetailAssembly {

    // MARK: - Properties
    private let navigationController: UINavigationController

    // MARK: - Initializers
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    // MARK: - Internal Methods
    func historicalPriceDetailPresenter(historicalPrice: Price) -> DetailPresenterProtocol {

        return HistoricalPriceDetailPresenter(historicalPrice: historicalPrice)
    }

    func detailNavigator() -> DetailNavigator {

        return PhoneDetailNavigator(navigationController: navigationController, viewControllerProvider: self)
    }
}

// MARK: - DetailViewControllerProvider
extension DetailAssembly: DetailViewControllerProvider {

    func detailViewController<T>(item: T) -> UIViewController? {

        var viewController: UIViewController?

        if let historicalPrice = item as? Price {
            let presenter = historicalPriceDetailPresenter(historicalPrice: historicalPrice)
            viewController = DetailViewController(presenter: presenter)
        }

        return viewController
    }
}
