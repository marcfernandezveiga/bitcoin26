//
//  CurrentPriceAssembly.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

public final class CurrentPriceAssembly {

    // MARK: - Properties
    private let webServiceAssembly: WebServiceAssembly

    // MARK: - Initializers
    public init(webServiceAssembly: WebServiceAssembly) {
        self.webServiceAssembly = webServiceAssembly
    }

    // MARK: - Public Methods
    public func currentPriceRepository() -> CurrentPriceRepositoryProtocol {

        return CurrentPriceRepository(webService: webServiceAssembly.webService)
    }

    // MARK: - Internal Methods
    func presenter() -> CurrentPricePresenterProtocol {

        return CurrentPricePresenter(currentPriceUpdate: .default, currentPriceRepository: currentPriceRepository())
    }

    func embededNavigator() -> EmbededNavigator {

        return CurrentPriceEmbededNavigator(viewControllerProvider: self)
    }
}

// MARK: - CurrentPriceViewControllerProvider
extension CurrentPriceAssembly: CurrentPriceViewControllerProvider {

    func currentPriceViewController() -> CurrentPriceViewController {

        return CurrentPriceViewController(presenter: presenter())
    }
}
