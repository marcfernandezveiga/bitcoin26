//
//  CurrentPriceRepository.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

public protocol CurrentPriceRepositoryProtocol {
    func currentPrice(completion completed: @escaping (Result<CurrentPrice, ServiceError>) -> Void)
}

internal final class CurrentPriceRepository: CurrentPriceRepositoryProtocol {

    // MARK: - Properties
    private let webService: WebService

    // MARK: - Initializers
    init(webService: WebService) {
        self.webService = webService
    }

    // MARK: - CurrentPriceRepositoryProtocol
    func currentPrice(completion completed: @escaping (Result<CurrentPrice, ServiceError>) -> Void) {

        webService.load(CurrentPriceServiceResponse.self,
                        from: .current) { result in

                            DispatchQueue.main.async {
                                switch result {
                                case .success(let soaCurrentPrice):
                                    let current = CurrentPriceBinding.bind(soaCurrentPrice)
                                    completed(.success(current))
                                case .failure(let error):
                                    completed(.failure(error))
                                }
                            }
        }
    }
}
