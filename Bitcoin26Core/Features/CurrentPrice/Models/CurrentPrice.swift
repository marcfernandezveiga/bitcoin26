//
//  CurrentPrice.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

// MARK: - Model
public struct CurrentPrice {
    let currencies: Currencies
}

struct Currencies {
    let euros: CurrencyDetail
    let dollars: CurrencyDetail
    let pounds: CurrencyDetail
}

struct CurrencyDetail {
    let code: String
    let symbol: String
    let value: Double
}

// MARK: - Service Model
struct CurrentPriceServiceResponse: Codable {

    let currencies: CurrenciesServiceResponse

    private enum CodingKeys: String, CodingKey {
        case currencies = "bpi"
    }
}

struct CurrenciesServiceResponse: Codable {

    let euros: CurrencyDetailServiceResponse
    let dollars: CurrencyDetailServiceResponse
    let pounds: CurrencyDetailServiceResponse

    private enum CodingKeys: String, CodingKey {
        case euros = "EUR"
        case dollars = "USD"
        case pounds = "GBP"
    }
}

struct CurrencyDetailServiceResponse: Codable {

    let code: String
    let symbol: String
    let value: Double

    private enum CodingKeys: String, CodingKey {
        case code
        case symbol
        case value = "rate_float"
    }
}

// MARK: - Binding class
internal final class CurrentPriceBinding {

    static func bind(_ soaCurrent: CurrentPriceServiceResponse) -> CurrentPrice {

        let soaEuros = soaCurrent.currencies.euros
        let soaDollars = soaCurrent.currencies.dollars
        let soaPounds = soaCurrent.currencies.pounds

        let euros = CurrencyDetail(code: soaEuros.code, symbol: soaEuros.symbol, value: soaEuros.value)
        let dollars = CurrencyDetail(code: soaDollars.code, symbol: soaDollars.symbol, value: soaDollars.value)
        let pounds = CurrencyDetail(code: soaPounds.code, symbol: soaPounds.symbol, value: soaPounds.value)

        let currencies = Currencies(euros: euros, dollars: dollars, pounds: pounds)

        return CurrentPrice(currencies: currencies)
    }
}
