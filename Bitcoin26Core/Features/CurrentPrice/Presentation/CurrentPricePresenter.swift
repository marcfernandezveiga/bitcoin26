//
//  CurrentPricePresenter.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

public protocol CurrentPriceView: class {
    func setLoading(_ loading: Bool)
    func showCurrentPrice(euros: String?, dollars: String?, pounds: String?)
    func showCountdown(time: Int?)
    func showReload()
}

public enum CurrentPriceUpdate {
    case none, `default`, short

    var timeInterval: TimeInterval? {
        switch self {
        case .none:
            return nil
        case .default:
            return 60
        case .short:
            return 10
        }
    }
}

public protocol CurrentPricePresenterProtocol: class {

    var view: CurrentPriceView? { get set }
    func loadView()
}

public final class CurrentPricePresenter: CurrentPricePresenterProtocol {

    // MARK: - Properties
    private let currentPriceUpdate: CurrentPriceUpdate
    private let currentPriceRepository: CurrentPriceRepositoryProtocol

    // MARK: - Variables
    public weak var view: CurrentPriceView?
    private var fetchTimer: Timer?
    private var countdownTimer: Timer?
    private var countdown: Int = 0

    // MARK: - Initializers
    public init(currentPriceUpdate: CurrentPriceUpdate, currentPriceRepository: CurrentPriceRepositoryProtocol) {
        self.currentPriceUpdate = currentPriceUpdate
        self.currentPriceRepository = currentPriceRepository
    }

    // MARK: - CurrentPricePresenterProtocol
    public func loadView() {

        fetchCurrent()
        restartFetchTimer()
        restartCountdownTimer()
    }

    // MARK: - Private Methods
    @objc private func fetchCurrent() {

        countdown = currentPriceUpdate.timeInterval.map { Int($0) } ?? 0
        view?.setLoading(true)
        currentPriceRepository.currentPrice { [weak self] result in

            guard let `self` = self else { return }
            self.view?.setLoading(false)
            switch result {
            case .success(let current):
                self.saveFactors(from: current)
                let euros = CurrencyManager.format(value: current.currencies.euros.value, currency: .euro)
                let dollars = CurrencyManager.format(value: current.currencies.dollars.value, currency: .dollar)
                let pounds = CurrencyManager.format(value: current.currencies.pounds.value, currency: .pound)
                self.view?.showCurrentPrice(euros: euros, dollars: dollars, pounds: pounds)
            case .failure:
                self.view?.showCurrentPrice(euros: nil, dollars: nil, pounds: nil)
                self.view?.showReload()
            }
        }
    }

    private func saveFactors(from currentPrice: CurrentPrice) {

        let euros = currentPrice.currencies.euros.value
        let dollars = currentPrice.currencies.dollars.value
        let pounds = currentPrice.currencies.pounds.value

        PersistentDataManager.eurosToDollarsFactor.value = dollars/euros
        PersistentDataManager.eurosToPoundsFactor.value = pounds/euros
    }

    private func restartFetchTimer() {

        fetchTimer?.invalidate()
        fetchTimer = nil
        startFetchTimer()
    }

    private func startFetchTimer() {

        if let timeInterval = currentPriceUpdate.timeInterval {
            fetchTimer = Timer.scheduledTimer(timeInterval: timeInterval,
                                              target: self,
                                              selector: #selector(fetchCurrent),
                                              userInfo: nil,
                                              repeats: true)
        }
    }

    private func restartCountdownTimer() {

        countdownTimer?.invalidate()
        countdownTimer = nil
        startCountdownTimer()
    }

    private func startCountdownTimer() {

        countdownTimer = Timer.scheduledTimer(timeInterval: 1,
                                              target: self,
                                              selector: #selector(showCountdown),
                                              userInfo: nil,
                                              repeats: true)
    }

    @objc private func showCountdown() {

        countdown -= 1
        if countdown > 0 {
            view?.showCountdown(time: countdown)
        }
    }
}
