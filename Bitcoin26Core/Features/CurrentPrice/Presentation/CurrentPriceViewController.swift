//
//  CurrentPriceViewController.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

internal protocol CurrentPriceViewControllerProvider: class {
    func currentPriceViewController() -> CurrentPriceViewController
}

internal final class CurrentPriceViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var countdownLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var eurosLabel: UILabel!
    @IBOutlet private weak var dollarsLabel: UILabel!
    @IBOutlet private weak var poundsLabel: UILabel!
    @IBOutlet private weak var reloadButton: UIButton!

    // MARK: - Properties
    private let presenter: CurrentPricePresenterProtocol

    // MARK: - Initializers
    init(presenter: CurrentPricePresenterProtocol) {
        self.presenter = presenter

        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.view = self

        configureUI()
        presenter.loadView()
    }

    // MARK: - Private Methods
    private func configureUI() {

        countdownLabel.text = nil
        activityIndicator.isHidden = true
        reloadButton.isHidden = true
    }

    // MARK: - IBActions
    @IBAction func reloadButtonTapped(_ sender: UIButton) {

        reloadButton.isHidden = true
        presenter.loadView()
    }
}

// MARK: - CurrentPriceView
extension CurrentPriceViewController: CurrentPriceView {

    func setLoading(_ loading: Bool) {

        activityIndicator.isHidden = false
        _ = loading ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }

    func showCurrentPrice(euros: String?, dollars: String?, pounds: String?) {

        reloadButton.isHidden = true
        eurosLabel.text = euros
        dollarsLabel.text = dollars
        poundsLabel.text = pounds
    }

    func showCountdown(time: Int?) {

        countdownLabel.text = activityIndicator.isAnimating ? nil : time.map { "\($0)" }
    }

    func showReload() {

        reloadButton.isHidden = false
    }
}
