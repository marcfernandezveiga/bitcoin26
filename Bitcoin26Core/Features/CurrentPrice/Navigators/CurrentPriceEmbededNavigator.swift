//
//  CurrentPriceEmbededNavigator.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

internal final class CurrentPriceEmbededNavigator: EmbededNavigator {

    // MARK: - Properties
    private unowned let viewControllerProvider: CurrentPriceViewControllerProvider

    // MARK: - Initializers
    init(viewControllerProvider: CurrentPriceViewControllerProvider) {
        self.viewControllerProvider = viewControllerProvider
    }

    // MARK: - EmbededNavigator
    func embedViewController(in containerView: UIView,
                             from viewController: UIViewController) {

        let currentPriceViewController = viewControllerProvider.currentPriceViewController()
        viewController.addChild(currentPriceViewController)
        containerView.fitSubview(currentPriceViewController.view)
        currentPriceViewController.didMove(toParent: viewController)
    }
}
