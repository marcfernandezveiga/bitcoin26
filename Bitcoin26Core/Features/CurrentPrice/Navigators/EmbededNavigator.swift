//
//  EmbededNavigator.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

public protocol EmbededNavigator {
    func embedViewController(in containerView: UIView,
                             from viewController: UIViewController)
}
