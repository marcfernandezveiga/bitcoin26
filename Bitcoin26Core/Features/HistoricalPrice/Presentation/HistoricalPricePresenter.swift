//
//  HistoricalPricePresenter.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

internal protocol HistoricalPriceView: class {
    var title: String? { get set }
    func setLoading(_ loading: Bool)
    func update(with prices: [Price])
    func showError(_ error: ServiceError)
}

internal protocol HistoricalPricePresenterProtocol: class {

    var view: HistoricalPriceView? { get set }
    func loadView()
    func embedCurrentPrice(in view: UIView, from viewController: UIViewController)
    func fetchHistoricalPrice()
    func didSelect(price: Price)
}

internal final class HistoricalPricePresenter: HistoricalPricePresenterProtocol {

    // MARK: - Properties
    private let embededNavigator: EmbededNavigator
    private let detailNavigator: DetailNavigator
    private let historicalPriceRepository: HistoricalPriceRepositoryProtocol

    // MARK: - Variables
    weak var view: HistoricalPriceView?

    // MARK: - Initializers
    init(embededNavigator: EmbededNavigator,
         detailNavigator: DetailNavigator,
         historicalPriceRepository: HistoricalPriceRepositoryProtocol) {
        self.embededNavigator = embededNavigator
        self.detailNavigator = detailNavigator
        self.historicalPriceRepository = historicalPriceRepository
    }

    // MARK: - HistoricalPricePresenterProtocol
    func loadView() {

        view?.title = "core_historicalPrice_title".localized
        fetchHistoricalPrice()
    }

    func embedCurrentPrice(in view: UIView, from viewController: UIViewController) {
        embededNavigator.embedViewController(in: view, from: viewController)
    }

    func fetchHistoricalPrice() {

        view?.setLoading(true)
        historicalPriceRepository.historicalPrice { [weak self] result in

            guard let `self` = self else { return }
            self.view?.setLoading(false)
            switch result {
            case .success(let historical):
                self.view?.update(with: historical.prices)
            case .failure(let error):
                self.view?.showError(error)
            }
        }
    }

    func didSelect(price: Price) {
        detailNavigator.showDetail(item: price)
    }
}
