//
//  HistoricalPriceViewController.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

internal final class HistoricalPriceViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var historicalTableView: UITableView!
    @IBOutlet private weak var errorView: UIView!
    @IBOutlet private weak var errorLabel: UILabel!
    @IBOutlet private weak var retryLabel: UILabel!

    // MARK: - Properties
    private let presenter: HistoricalPricePresenterProtocol

    // MARK: - Constants
    private enum Constants {
        static let activityIndicatorHeight: CGFloat = 40
        static let retryButtonAnimationDuration: TimeInterval = 0.5
        static let retryButtonAlpha: CGFloat = 0.2
    }

    // MARK: - Variables
    private var prices: [Price] = []
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = #colorLiteral(red: 0.1921568627, green: 0.631372549, blue: 0.5411764706, alpha: 1)
        return refreshControl
    }()
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let frame = CGRect(x: 0,
                           y: 0,
                           width: historicalTableView.bounds.width,
                           height: Constants.activityIndicatorHeight)
        let activityIndicator = UIActivityIndicatorView(frame: frame)
        activityIndicator.color = #colorLiteral(red: 0.1921568627, green: 0.631372549, blue: 0.5411764706, alpha: 1)
        activityIndicator.tintColor = #colorLiteral(red: 0.1921568627, green: 0.631372549, blue: 0.5411764706, alpha: 1)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        return activityIndicator
    }()

    // MARK: - Initializers
    init(presenter: HistoricalPricePresenterProtocol) {
        self.presenter = presenter

        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.view = self

        configureUI()
        presenter.embedCurrentPrice(in: containerView, from: self)
        presenter.loadView()
    }

    // MARK: - Private Methods
    private func configureUI() {

        historicalTableView.accessibilityIdentifier = "HistoricalPriceTableView"
        historicalTableView.register(HistoricalPriceCell.self)
        historicalTableView.tableFooterView = activityIndicator

        refreshControl.addTarget(self, action: #selector(refreshHistorical), for: .valueChanged)
        historicalTableView.addSubview(refreshControl)

        errorView.isHidden = true
    }

    @objc private func refreshHistorical() {

        presenter.fetchHistoricalPrice()
    }

    private func animateRetryButton() {

        UIView.animate(withDuration: Constants.retryButtonAnimationDuration,
                       delay: 0,
                       options: [.autoreverse, .repeat],
                       animations: { [weak self] in

                        self?.retryLabel.alpha = Constants.retryButtonAlpha
        })
    }

    // MARK: - IBActions
    @IBAction func retryButtonTapped(_ sender: UIButton) {

        errorView.isHidden = true
        refreshHistorical()
    }
}

// MARK: - HistoricalPriceView
extension HistoricalPriceViewController: HistoricalPriceView {

    func setLoading(_ loading: Bool) {

        _ = loading ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }

    func update(with prices: [Price]) {

        self.prices = prices
        historicalTableView.reloadData()

        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }

    func showError(_ error: ServiceError) {

        errorLabel.text = error.localizedDescription
        animateRetryButton()
        errorView.isHidden = false
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate
extension HistoricalPriceViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {

        return prices.count
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(HistoricalPriceCell.self, for: indexPath)
        cell.bind(with: prices[indexPath.row])
        cell.accessibilityIdentifier = "HistoricalPriceCell_\(indexPath.row)"
        return cell
    }

    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {

        let price = prices[indexPath.row]
        presenter.didSelect(price: price)
    }
}
