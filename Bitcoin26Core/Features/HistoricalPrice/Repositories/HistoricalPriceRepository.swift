//
//  HistoricalPriceRepository.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

internal protocol HistoricalPriceRepositoryProtocol {
    func historicalPrice(completion completed: @escaping (Result<HistoricalPrice, ServiceError>) -> Void)
}

internal final class HistoricalPriceRepository: HistoricalPriceRepositoryProtocol {

    // MARK: - Properties
    private let webService: WebService

    // MARK: - Initializers
    init(webService: WebService) {
        self.webService = webService
    }

    // MARK: - HistoricalPriceRepositoryProtocol
    func historicalPrice(completion completed: @escaping (Result<HistoricalPrice, ServiceError>) -> Void) {

        webService.load(HistoricalPriceServiceResponse.self,
                        from: .historical(currencyCode: CurrencyManager.defaultCurrency.code)) { result in

                            DispatchQueue.main.async {
                                switch result {
                                case .success(let soaHistoricalPrice):
                                    let historical = HistoricalPriceBinding.bind(soaHistoricalPrice)
                                    completed(.success(historical))
                                case .failure(let error):
                                    completed(.failure(error))
                                }
                            }
        }
    }
}
