//
//  HistoricalPriceAssembly.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

public final class HistoricalPriceAssembly {

    // MARK: - Properties
    private let currentPriceAssembly: CurrentPriceAssembly
    private let detailAssembly: DetailAssembly
    private let webServiceAssembly: WebServiceAssembly

    // MARK: - Initializers
    init(currentPriceAssembly: CurrentPriceAssembly,
         detailAssembly: DetailAssembly,
         webServiceAssembly: WebServiceAssembly) {
        self.currentPriceAssembly = currentPriceAssembly
        self.detailAssembly = detailAssembly
        self.webServiceAssembly = webServiceAssembly
    }

    // MARK: - Public Methods
    public func viewController() -> UIViewController {

        return HistoricalPriceViewController(presenter: presenter())
    }

    // MARK: - Internal Methods
    func presenter() -> HistoricalPricePresenterProtocol {

        return HistoricalPricePresenter(embededNavigator: currentPriceAssembly.embededNavigator(),
                                        detailNavigator: detailAssembly.detailNavigator(),
                                        historicalPriceRepository: historicalPriceRepository())
    }

    func historicalPriceRepository() -> HistoricalPriceRepositoryProtocol {

        return HistoricalPriceRepository(webService: webServiceAssembly.webService)
    }
}
