//
//  HistoricalPrice.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

// MARK: - Model
struct HistoricalPrice {
    let prices: [Price]
}

struct Price {
    let value: Double
    let date: Date
    var state: PriceState?
}

internal enum PriceState {
    case increased, decreased, maintained
}

// MARK: - Service Model
struct HistoricalPriceServiceResponse: Codable {

    let prices: [String: Double]

    private enum CodingKeys: String, CodingKey {
        case prices = "bpi"
    }
}

// MARK: - Binding class
internal final class HistoricalPriceBinding {

    static func bind(_ soaHistoricalPrice: HistoricalPriceServiceResponse) -> HistoricalPrice {

        var prices: [Price] = []

        soaHistoricalPrice.prices.forEach { soaPrice in

            let value = soaPrice.value
            let date = DateManager.date(from: soaPrice.key, format: .yyyy_MM_dd)
            let price = Price(value: value, date: date)
            prices.append(price)
        }

        prices.sort(by: { $0.date.compare($1.date) == .orderedDescending })

        for index in 0..<(prices.count - 1) {

            if prices[index].value > prices[index + 1].value {
                prices[index].state = .increased
            } else if prices[index].value < prices[index + 1].value {
                prices[index].state = .decreased
            } else {
                prices[index].state = .maintained
            }
        }

        return HistoricalPrice(prices: prices)
    }
}
