//
//  HistoricalCellPrice.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

internal final class HistoricalPriceCell: UITableViewCell, NibLoadableView, ReusableCell {

    // MARK: - IBOutlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var stateImageView: UIImageView!

    // MARK: - Lifecycle Methods
    override func prepareForReuse() {
        super.prepareForReuse()

        dateLabel.text = nil
        priceLabel.text = nil
        stateImageView.image = nil
    }

    // MARK: - Internal Methods
    func bind(with price: Price) {

        dateLabel.text = DateManager.string(from: price.date, format: .ddMMyyyy)
        priceLabel.text = CurrencyManager.format(value: price.value)

        guard let state = price.state else { return }
        var imageName: String
        switch state {
        case .increased:
            imageName = "increased"
        case .decreased:
            imageName = "decreased"
        case .maintained:
            imageName = "maintained"
        }

        stateImageView.image = UIImage(named: imageName,
                                       in: Bundle(for: HistoricalPriceCell.self),
                                       compatibleWith: nil)
    }
}
