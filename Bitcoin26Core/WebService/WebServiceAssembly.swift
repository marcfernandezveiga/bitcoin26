//
//  WebServiceAssembly.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

public final class WebServiceAssembly {

    // MARK: - Variables
    private(set) lazy var webService = WebService(session: URLSession(configuration: .default))

    // MARK: - Initializers
    public init() {}
}
