//
//  Endpoint.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 06/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

internal enum Endpoint {
    case current
    case historical(currencyCode: String)
}

internal extension Endpoint {

    func request(with baseURL: URL, adding parameters: [String: String]) -> URLRequest {
        let url = baseURL.appendingPathComponent(path)

        var newParameters = self.parameters
        parameters.forEach { newParameters.updateValue($1, forKey: $0) }

        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        components.queryItems = newParameters.map(URLQueryItem.init)

        var request = URLRequest(url: components.url!)
        request.httpMethod = method.rawValue

        return request
    }
}

private enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}

// MARK: - Endpoint variables
private extension Endpoint {

    var method: HTTPMethod {
        switch self {
        case .current:
            return .get
        case .historical:
            return .get
        }
    }

    var path: String {
        switch self {
        case .current:
            return "currentprice.json"
        case .historical:
            return "historical/close.json"
        }
    }

    var parameters: [String: String] {
        switch self {
        case .current:
            return [:]
        case .historical(let currencyCode):
            return ["currency": currencyCode]
        }
    }
}
