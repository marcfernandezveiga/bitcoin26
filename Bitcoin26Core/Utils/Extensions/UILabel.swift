//
//  UILabel.swift
//  Bitcoin26
//
//  Created by Marc Fernandez Veiga on 11/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

public extension UILabel {

    // MARK: - UILabel + localized
    @IBInspectable var localizedText: String {
        get {
            return text ?? ""
        }
        set {
            text = newValue.localized
        }
    }
}
