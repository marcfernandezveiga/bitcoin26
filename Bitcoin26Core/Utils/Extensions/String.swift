//
//  String.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 11/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

public extension String {

    private enum Constants {
        static let bundleIdentifierBase: String = "com.marcfernandezveiga.Bitcoin26"
    }

    // MARK: - String + localized
    var localized: String {

        var bundle: Bundle?
        if let bundleName = self.split(separator: "_").first {
            if bundleName == "app" {
                bundle = .main
            } else {
                bundle = Bundle(identifier: "\(Constants.bundleIdentifierBase).\(bundleName)")
            }
        }

        return NSLocalizedString(self,
                                 tableName: "Localizable",
                                 bundle: bundle ?? .main,
                                 value: "**\(self)**",
                                 comment: "")
    }
}
