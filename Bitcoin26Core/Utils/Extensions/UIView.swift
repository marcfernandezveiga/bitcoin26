//
//  UIView.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

public extension UIView {

    // MARK: - UIView + IBInspectable
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable
    var shadowColor: UIColor? {
        get {
            return layer.shadowColor.flatMap { UIColor(cgColor: $0) }
        }
        set {
            layer.shadowOffset = CGSize(width: 2, height: 2)
            layer.shadowColor = newValue?.cgColor
            layer.shadowOpacity = 0.5
        }
    }

    // MARK: - UIView + fitSubview
    func fitSubview(_ view: UIView) {

        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false

        let views = ["view": view]
        var allConstraints = [NSLayoutConstraint]()

        let fillParentVertConst = NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: views)
        allConstraints += fillParentVertConst

        let fillParentHorizConsts = NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: views)
        allConstraints += fillParentHorizConsts

        NSLayoutConstraint.activate(allConstraints)
    }
}
