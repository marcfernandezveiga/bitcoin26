//
//  Bundle.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 11/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

public extension Bundle {

    // MARK: - Variables
    static var core: Bundle? {
        return Bundle(identifier: "com.marcfernandezveiga.Bitcoin26.core")
    }
}
