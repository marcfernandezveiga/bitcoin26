//
//  DateManager.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

// MARK: - DateFormat
internal enum DateFormat: String {
    case yyyy_MM_dd = "yyyy-MM-dd"
    case ddMMyyyy = "dd/MM/yyyy"
    case MMddyyyy = "MM/dd/yyyy"
}

// MARK: - DateManager
internal final class DateManager {

    // MARK: - Internal Methods
    class func date(from string: String, format: DateFormat) -> Date {

        let formatter = DateFormatter()
        formatter.dateFormat = format.rawValue
        return formatter.date(from: string)!
    }

    class func string(from date: Date, format: DateFormat) -> String {

        let formatter = DateFormatter()
        formatter.dateFormat = format.rawValue
        return formatter.string(from: date)
    }
}
