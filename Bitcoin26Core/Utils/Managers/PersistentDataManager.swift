//
//  PersistentDataManager.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 10/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

// MARK: - PersistentKey
private enum PersistentKey: String {
    case eurosToDollarsFactor, eurosToPoundsFactor
}

// MARK: - PersistentDataManager
internal final class PersistentDataManager {

    internal static var eurosToDollarsFactor = Persistent<Double>(.eurosToDollarsFactor)
    internal static var eurosToPoundsFactor = Persistent<Double>(.eurosToPoundsFactor)
}

// MARK: - Persistent
internal struct Persistent<T> {

    // MARK: - Properties
    private let defaults = UserDefaults.standard
    private let key: String

    // MARK: - Initializers
    fileprivate init(_ key: PersistentKey) {
        self.key = key.rawValue
    }

    // MARK: - Variables
    var value: T? {
        get {
            return defaults.value(forKey: key) as? T
        }
        set {
            guard newValue != nil else {
                defaults.removeObject(forKey: key)
                return
            }
            defaults.set(newValue, forKey: key)
        }
    }
}
