//
//  CurrencyManager.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import Foundation

// MARK: - Currency
internal enum Currency {
    case euro, dollar, pound

    var code: String {
        switch self {
        case .euro:
            return "EUR"
        case .dollar:
            return "USD"
        case .pound:
            return "GBP"
        }
    }
}

// MARK: - CurrencyManager
internal final class CurrencyManager {

    // MARK: - Constants
    static let defaultCurrency: Currency = .euro

    // MARK: - Internal Methods
    class func format(value: Double,
                      currency: Currency = defaultCurrency) -> String? {

        let formatter = NumberFormatter()

        var localeIdentifier: String
        switch currency {
        case .euro:
            localeIdentifier = "es_ES"
        case .dollar:
            localeIdentifier = "en_US"
        case .pound:
            localeIdentifier = "en_GB"
        }

        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.allowsFloats = true
        formatter.alwaysShowsDecimalSeparator = true
        formatter.currencyCode = currency.code
        formatter.numberStyle = .currency
        formatter.usesGroupingSeparator = true
        formatter.maximumFractionDigits = 2
        return formatter.string(from: NSNumber(value: value))
    }

    class func convertToDollars(euros: Double) -> Double? {

        var dollars: Double?
        if let factor = PersistentDataManager.eurosToDollarsFactor.value {
            dollars = euros * factor
        }
        return dollars
    }

    class func convertToPounds(euros: Double) -> Double? {

        var pounds: Double?
        if let factor = PersistentDataManager.eurosToPoundsFactor.value {
            pounds = euros * factor
        }
        return pounds
    }
}
