//
//  ReusableCell.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

public protocol ReusableCell: class {
    static var reuseIdentifier: String { get }
}

public extension ReusableCell where Self: UITableViewCell {

    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
