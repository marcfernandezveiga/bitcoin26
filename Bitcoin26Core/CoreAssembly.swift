//
//  CoreAssembly.swift
//  Bitcoin26Core
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

final public class CoreAssembly {

    // MARK: - Properties
    private let navigationController: UINavigationController

    // MARK: - Variables
    public private(set) lazy var historicalPriceAssembly =
        HistoricalPriceAssembly(currentPriceAssembly: currentPriceAssembly,
                                detailAssembly: detailAssembly,
                                webServiceAssembly: webServiceAssembly)
    private(set) lazy var currentPriceAssembly = CurrentPriceAssembly(webServiceAssembly: webServiceAssembly)
    private(set) lazy var detailAssembly = DetailAssembly(navigationController: navigationController)
    private(set) lazy var webServiceAssembly = WebServiceAssembly()

    // MARK: - Initializers
    public init(navigationController: UINavigationController) {

        self.navigationController = navigationController
    }
}
