# Table of contents
1. [Requirements](#requirements)
2. [Description](#description)
    *  [Application](#application)
    *  [Project structure](#project-structure)
    *  [Architecture](#architecture)
3. [Numbers](#numbers)
    *  [Test coverage](#test-coverage)
    *  [Swiftlint violations](#swiftlint-violations)

## Requirements
Current supported version for Bitcoin26:

* **Minimum OS**: iOS 11
* **Swift**: Swift 5

## Description

### Application

In this app you'll see the current exchange rate of Bitcoin in EUR, USD and GBP. You'll also have a list of the exchange rate for the previous 31 days. Moreover, there's a Today Widget which will display the current exchange rate only in EUR and updated every 10 seconds. This app supports Dark Mode:

**Current exchange rate and historical**

<img src="pictures/B26_historicalPrice_light.png"><img src="pictures/B26_historicalPrice_dark.png">

**Exchange rate detal**

<img src="pictures/B26_detail_light.png"><img src="pictures/B26_detail_dark.png">

**Today's exchange rate widget**

<img src="pictures/B26_widget_light.png"><img src="pictures/B26_widget_dark.png">

### Project structure

A good practice when building an iOS app is working with different modules. This helps having control of the different dependencies, reuse code between the project and avoid conflicts between team members between others. In this case, because there's a TodayExtension that shows the same information as our main app screen (the current exchange rate of Bitcoin), I knew that code was going to be reused. Therefore, the project is structured in 3 different targets: the Bitcoin26 main app, the Bitcoin26Core framework and the Bitcoin26TodayExtension. In this case, the Bitcoin26 app will only initialize the app and it is the Bitcoin26Core framework who has all the app's logic. Also, the different targets will have their testing bundles.

### Architecture

One of the requierements for this challenge was to implement a simple, not overengineered solution. The app only has two main views and it would have been really easy to have a simple storyboard with a segue, implement some MVC or MVP architecture and that's it. However, in order to show some of my iOS skills and my understanding of iOS programming I wanted to go one step further by implementing a kind of more layered MVP architecture. To understand the differences, please see the diagrams below:

**Regular MVP**

<img src="pictures/mvp.png">

**My architecture**

<img src="pictures/mvp_steroids.png">

As you can see, with this approach we have and architecture separated in more layers, each assuming the least about it's environment. Each module will have and Assembly which will be responsible for building all the different layers in that module using dependency injection. The different assemblies can have dependencies between them to access the layers from other modules. So, we have an AppAssembly which will be initialized when the app launches. This will have a dependency with the CoreAssembly that will have a dependency with the HistoricalPriceAssembly, the CurrentPriceAssembly, the DetailAssembly and the WebServiceAssembly. 

Each assembly will be initialized with the dependencies it needs so, for example, the HistoricalPriceAssembly will need a dependency with the WebServiceAssembly in order to inject the web service to it's repository and with the CurrentPriceAssembly and the DetailAssembly to inject the navigator to it's presenter. Therefore, our HistoricalPriceAssembly will look something like this:

```swift
public final class HistoricalPriceAssembly {

    // MARK: - Properties
    private let currentPriceAssembly: CurrentPriceAssembly
    private let detailAssembly: DetailAssembly
    private let webServiceAssembly: WebServiceAssembly

    // MARK: - Initializers
    init(currentPriceAssembly: CurrentPriceAssembly,
         detailAssembly: DetailAssembly,
         webServiceAssembly: WebServiceAssembly) {
        self.currentPriceAssembly = currentPriceAssembly
        self.detailAssembly = detailAssembly
        self.webServiceAssembly = webServiceAssembly
    }

    // MARK: - Public Methods
    public func viewController() -> UIViewController {

        return HistoricalPriceViewController(presenter: presenter())
    }

    // MARK: - Internal Methods
    func presenter() -> HistoricalPricePresenterProtocol {

        return HistoricalPricePresenter(embededNavigator: currentPriceAssembly.embededNavigator(),
                                        detailNavigator: detailAssembly.detailNavigator(),
                                        historicalPriceRepository: historicalPriceRepository())
    }

    func historicalPriceRepository() -> HistoricalPriceRepositoryProtocol {

        return HistoricalPriceRepository(webService: webServiceAssembly.webService)
    }
}
```

As you can see, the app will finally look like a puzzle. We only need to create our Assembly, which will build it's ViewController, Presenter, Repository and Navigator and inject whatever assembly we need to the other assemblies. With this, we distribute the responsibilities, we improve the testability and we keep it simple.

## Numbers

### Test coverage

For this app, I included two test bundles: Unit tests for the Bitcoin26Core framework and UI tests for the Bitcoin26 app. Almost all of the app has been tested, leading to a **94% of test coverage**. There were some parts that weren't tested such as fatalErrors in initializers of views with NSCoder.

### Swiftlint violations

This app only has 1 swiftlint violation. The [Coindesk API](https://www.coindesk.com/api) returns the dates in a string format of "yyyy-MM-dd". For better usage, in the app I worked with Date, not with Strings. Therefore, I build a DateManager responsible for making these conversions from Date to String and viceversa to use around the app. This DateManager uses an internal enum DateFormat which has the different date formats you can come across within Bitcoin26: **yyyy_MM_dd**("yyyy-MM-dd"), **ddMMyyyy**("dd/MM/yyyy") and **MMddyyyy**("MM/dd/yyyy"). Because of this, the only violation is:

*Identifier Name Violation: Enum element name should only contain alphanumeric characters: 'yyyy_MM_dd' (identifier_name)*