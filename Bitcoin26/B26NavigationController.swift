//
//  B26NavigationController.swift
//  Bitcoin26
//
//  Created by Marc Fernandez Veiga on 10/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

internal final class B26NavigationController: UINavigationController, UINavigationControllerDelegate {

    // MARK: - Constants
    private enum Constants {
        static let navigationBarTintColor = UIColor(named: "B26DarkText",
                                                    in: Bundle.main,
                                                    compatibleWith: nil)
        static let navigationBarTitleFont = UIFont(name: "Futura-Medium", size: 16)!
    }

    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        configureUI()
    }

    // MARK: - Private Methods
    private func configureUI() {

        navigationBar.tintColor = Constants.navigationBarTintColor
        navigationBar.titleTextAttributes = [.font: Constants.navigationBarTitleFont]
    }

    // MARK: - UINavigationControllerDelegate
    func navigationController(_ navigationController: UINavigationController,
                              willShow viewController: UIViewController,
                              animated: Bool) {

        let item = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        viewController.navigationItem.backBarButtonItem = item
    }
}
