//
//  AppAssembly.swift
//  Bitcoin26
//
//  Created by Marc Fernandez Veiga on 07/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit
import Bitcoin26Core

internal final class AppAssembly {

    // MARK: - Variables
    private(set) lazy var window = UIWindow(frame: UIScreen.main.bounds)
    private(set) lazy var coreAssembly = CoreAssembly(navigationController: navigationController)
    private(set) lazy var navigationController: UINavigationController = {
        return B26NavigationController()
    }()
}
