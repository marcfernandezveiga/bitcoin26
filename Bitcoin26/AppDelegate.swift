//
//  AppDelegate.swift
//  Bitcoin26
//
//  Created by Marc Fernandez Veiga on 06/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Properties
    let appAssembly = AppAssembly()

    // MARK: - Lifecycle Methods
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let initialViewController = appAssembly.coreAssembly.historicalPriceAssembly.viewController()

        appAssembly.navigationController.pushViewController(initialViewController,
                                                            animated: false)

        appAssembly.window.rootViewController = appAssembly.navigationController

        appAssembly.window.makeKeyAndVisible()

        return true
    }
}
