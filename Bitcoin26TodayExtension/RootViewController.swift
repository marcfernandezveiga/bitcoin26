//
//  TodayViewController.swift
//  Bitcoin26TodayExtension
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit
import NotificationCenter
import Bitcoin26Core

internal final class RootViewController: UIViewController, NCWidgetProviding {

    // MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!

    // MARK: - Properties
    private let todayAssembly = TodayAssembly()

    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        todayAssembly.embededNavigator().embedViewController(in: containerView,
                                                             from: self)
    }

    // MARK: - NCWidgetProviding
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {

        completionHandler(NCUpdateResult.newData)
    }
}
