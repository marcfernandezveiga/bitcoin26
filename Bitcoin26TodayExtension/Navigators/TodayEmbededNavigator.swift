//
//  TodayPriceEmbededNavigator.swift
//  Bitcoin26TodayExtension
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit
import Bitcoin26Core

internal final class TodayEmbededNavigator: EmbededNavigator {

    // MARK: - Properties
    private unowned let viewControllerProvider: TodayViewControllerProvider

    // MARK: - Initializers
    init(viewControllerProvider: TodayViewControllerProvider) {
        self.viewControllerProvider = viewControllerProvider
    }

    // MARK: - EmbededNavigator
    func embedViewController(in containerView: UIView,
                             from viewController: UIViewController) {

        let todayViewController = viewControllerProvider.todayViewController()
        viewController.addChild(todayViewController)
        containerView.fitSubview(todayViewController.view)
        todayViewController.didMove(toParent: viewController)
    }
}
