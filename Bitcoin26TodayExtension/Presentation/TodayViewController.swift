//
//  TodayViewController.swift
//  Bitcoin26TodayExtension
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit
import Bitcoin26Core

internal protocol TodayViewControllerProvider: class {
    func todayViewController() -> TodayViewController
}

class TodayViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var countdownLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var eurosLabel: UILabel!

    // MARK: - Properties
    private let presenter: CurrentPricePresenterProtocol

    // MARK: - Initializers
    init(presenter: CurrentPricePresenterProtocol) {
        self.presenter = presenter

        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.view = self

        configureUI()
        presenter.loadView()
    }

    // MARK: - Private Methods
    private func configureUI() {

        countdownLabel.text = nil
        activityIndicator.isHidden = true
    }
}

// MARK: - CurrentPriceView
extension TodayViewController: CurrentPriceView {

    func setLoading(_ loading: Bool) {

        activityIndicator.isHidden = false
        _ = loading ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }

    func showCurrentPrice(euros: String?, dollars: String?, pounds: String?) {

        eurosLabel.text = euros
    }

    func showCountdown(time: Int?) {

        countdownLabel.text = activityIndicator.isAnimating ? nil : time.map { "\($0)" }
    }

    func showReload() {

        eurosLabel.text = "todayExtension_today_loading".localized
    }
}
