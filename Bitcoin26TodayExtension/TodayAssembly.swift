//
//  TodayAssembly.swift
//  Bitcoin26TodayExtension
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit
import Bitcoin26Core

internal final class TodayAssembly {

    // MARK: - Variables
    private(set) lazy var currentPriceAssembly = CurrentPriceAssembly(webServiceAssembly: webServiceAssembly)
    private(set) lazy var webServiceAssembly = WebServiceAssembly()

    // MARK: - Internal Methods
    func presenter() -> CurrentPricePresenterProtocol {

        return CurrentPricePresenter(currentPriceUpdate: .short,
                                     currentPriceRepository: currentPriceAssembly.currentPriceRepository())
    }

    func embededNavigator() -> EmbededNavigator {

        return TodayEmbededNavigator(viewControllerProvider: self)
    }
}

// MARK: - TodayViewControllerProvider
extension TodayAssembly: TodayViewControllerProvider {

    func todayViewController() -> TodayViewController {

        return TodayViewController(presenter: presenter())
    }
}
