//
//  TestCell.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import UIKit
import Bitcoin26Core

internal final class TestCell: UITableViewCell, NibLoadableView, ReusableCell {

    // MARK: - IBOutlets
    @IBOutlet weak var testLabel: UILabel!
}
