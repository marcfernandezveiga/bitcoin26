//
//  HistoricalPriceDetailPresenterTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 10/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

// MARK: - DetailViewMock
class DetailViewMock: DetailView {

    private(set) var showDateCalled = false
    private(set) var showPriceCalled = false

    var title: String?

    func show(date: String) {
        showDateCalled = true
    }

    func showPrice(euros: String?, dollars: String?, pounds: String?) {
        showPriceCalled = true
    }
}

// MARK: - HistoricalPriceDetailPresenterTests
class HistoricalPriceDetailPresenterTests: XCTestCase {

    // MARK: - Variables
    private var view: DetailViewMock!
    private var presenter: HistoricalPriceDetailPresenter!

    // MARK: - Tests
    func test_loadView() {

        // Given
        view = DetailViewMock()
        let price = Price(value: 100, date: Date())
        presenter = HistoricalPriceDetailPresenter(historicalPrice: price)
        presenter.view = view

        // When
        presenter.loadView()

        // Then
        XCTAssertEqual(view.title, "Detail")
        XCTAssertTrue(view.showDateCalled)
        XCTAssertTrue(view.showPriceCalled)
    }
}
