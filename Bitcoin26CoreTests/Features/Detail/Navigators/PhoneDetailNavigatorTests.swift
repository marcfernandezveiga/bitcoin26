//
//  PhoneDetailNavigatorTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 10/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class DetailViewControllerProviderMock: DetailViewControllerProvider {

    func detailViewController<T>(item: T) -> UIViewController? {
        guard let price = item as? Price else { return nil }
        let presenter = HistoricalPriceDetailPresenter(historicalPrice: price)
        return DetailViewController(presenter: presenter)
    }
}

class PhoneDetailNavigatorTests: XCTestCase {

    // MARK: - Variables
    private var navigationController: UINavigationController!
    private var viewControllerProvider: DetailViewControllerProviderMock!
    private var navigator: PhoneDetailNavigator!

    func test_showDetail() {

        // Given
        navigationController = UINavigationController()
        viewControllerProvider = DetailViewControllerProviderMock()
        navigator = PhoneDetailNavigator(navigationController: navigationController,
                                         viewControllerProvider: viewControllerProvider)

        // When
        let price = Price(value: 100, date: Date())
        navigator.showDetail(item: price)

        // Then
        XCTAssertTrue(navigationController.visibleViewController is DetailViewController)
    }
}
