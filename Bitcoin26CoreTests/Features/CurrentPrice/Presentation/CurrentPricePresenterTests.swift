//
//  CurrentPricePresenterTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

// MARK: - CurrentPriceViewMock
class CurrentPriceViewMock: CurrentPriceView {

    private(set) var startLoadingCalled = false
    private(set) var stopLoadingCalled = false
    private(set) var showCurrentPriceCalled = false
    private(set) var showCountdownCalled = false
    private(set) var showReloadCalled = false

    func setLoading(_ loading: Bool) {
        if loading { startLoadingCalled = true } else { stopLoadingCalled = true }
    }

    func showCurrentPrice(euros: String?, dollars: String?, pounds: String?) {
        showCurrentPriceCalled = true
    }

    func showCountdown(time: Int?) {
        showCountdownCalled = true
    }

    func showReload() {
        showReloadCalled = true
    }
}

// MARK: - CurrentPriceRepositoryMock
class CurrentPriceRepositoryMock: CurrentPriceRepositoryProtocol {

    var forceError = false

    func currentPrice(completion completed: @escaping (Result<CurrentPrice, ServiceError>) -> Void) {

        let euros = CurrencyDetail(code: "EUR", symbol: "€", value: 9237.9529)
        let dollars = CurrencyDetail(code: "USD", symbol: "$", value: 7490.0605)
        let pounds = CurrencyDetail(code: "GBP", symbol: "£", value: 9426.5109)
        let currencies = Currencies(euros: euros, dollars: dollars, pounds: pounds)
        let currentPrice = CurrentPrice(currencies: currencies)
        let serviceError = ServiceError.unexpected

        _ = forceError ? completed(.failure(serviceError)) : completed(.success(currentPrice))
    }
}

// MARK: - CurrentPricePresenterTests
class CurrentPricePresenterTests: XCTestCase {

    // MARK: - Variables
    private var repository: CurrentPriceRepositoryMock!
    private var view: CurrentPriceViewMock!
    private var presenter: CurrentPricePresenter!

    // MARK: - Tests
    func test_loadView() {

        // Given
        repository = CurrentPriceRepositoryMock()
        repository.forceError = false
        view = CurrentPriceViewMock()
        presenter = CurrentPricePresenter(currentPriceUpdate: .default, currentPriceRepository: repository)
        presenter.view = view

        // When
        presenter.loadView()

        // Then
        XCTAssertTrue(view.startLoadingCalled)
        XCTAssertTrue(view.stopLoadingCalled)
        XCTAssertTrue(view.showCurrentPriceCalled)
    }

    func test_fetchCurrent_fail() {

        // Given
        repository = CurrentPriceRepositoryMock()
        repository.forceError = true
        view = CurrentPriceViewMock()
        presenter = CurrentPricePresenter(currentPriceUpdate: .short,
                                          currentPriceRepository: repository)
        presenter.view = view

        // When
        presenter.loadView()

        // Then
        XCTAssertTrue(view.startLoadingCalled)
        XCTAssertTrue(view.stopLoadingCalled)
        XCTAssertTrue(view.showReloadCalled)
    }
}
