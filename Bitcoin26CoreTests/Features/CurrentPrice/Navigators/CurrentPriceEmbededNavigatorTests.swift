//
//  CurrentPriceEmbededNavigatorTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class CurrentPriceViewControllerProviderMock: CurrentPriceViewControllerProvider {

    func currentPriceViewController() -> CurrentPriceViewController {
        let repository = CurrentPriceRepositoryMock()
        let presenter = CurrentPricePresenter(currentPriceUpdate: .none, currentPriceRepository: repository)
        return CurrentPriceViewController(presenter: presenter)
    }
}

class CurrentEmbededNavigatorTests: XCTestCase {

    // MARK: - Variables
    private var viewControllerProvider: CurrentPriceViewControllerProviderMock!
    private var navigator: CurrentPriceEmbededNavigator!

    func test_embed() {

        // Given
        viewControllerProvider = CurrentPriceViewControllerProviderMock()
        navigator = CurrentPriceEmbededNavigator(viewControllerProvider: viewControllerProvider)

        // When
        let viewController = UIViewController()
        navigator.embedViewController(in: viewController.view, from: viewController)

        // Then
        XCTAssertFalse(viewController.children.isEmpty)
        XCTAssertTrue(viewController.children.first is CurrentPriceViewController)
    }
}
