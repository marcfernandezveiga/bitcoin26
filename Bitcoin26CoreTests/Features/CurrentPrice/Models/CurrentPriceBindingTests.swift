//
//  CurrentPriceBindingTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class CurrentPriceBindingTests: XCTestCase {

    func test_binding() {

        let soaEuros = CurrencyDetailServiceResponse(code: "EUR", symbol: "€", value: 9237.9529)
        let soaDollars = CurrencyDetailServiceResponse(code: "USD", symbol: "$", value: 7490.0605)
        let soaPounds = CurrencyDetailServiceResponse(code: "GBP", symbol: "£", value: 9426.5109)
        let soaCurrencies = CurrenciesServiceResponse(euros: soaEuros, dollars: soaDollars, pounds: soaPounds)
        let soaCurrentPrice = CurrentPriceServiceResponse(currencies: soaCurrencies)

        let currentPrice = CurrentPriceBinding.bind(soaCurrentPrice)

        XCTAssertEqual(currentPrice.currencies.euros.value, 9237.9529)
        XCTAssertEqual(currentPrice.currencies.euros.code, "EUR")
        XCTAssertEqual(currentPrice.currencies.euros.symbol, "€")
        XCTAssertEqual(currentPrice.currencies.dollars.value, 7490.0605)
        XCTAssertEqual(currentPrice.currencies.dollars.code, "USD")
        XCTAssertEqual(currentPrice.currencies.dollars.symbol, "$")
        XCTAssertEqual(currentPrice.currencies.pounds.value, 9426.5109)
        XCTAssertEqual(currentPrice.currencies.pounds.code, "GBP")
        XCTAssertEqual(currentPrice.currencies.pounds.symbol, "£")
    }
}
