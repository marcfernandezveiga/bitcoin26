//
//  CurrentPriceRepositoryTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 09/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class CurrentPriceRepositoryTests: XCTestCase {

    // MARK: - Constants
    private enum Constants {
        static let baseURL: URL = URL(string: "www.test.com")!
        static let timeout: TimeInterval = 5
    }

    // MARK: - Variables
    private var session: URLSessionMock!
    private var webService: WebService!
    private var repository: CurrentPriceRepository!

    func test_CurrentPrice_success() {

        // Given
        session = URLSessionMock()
        let response = HTTPURLResponse(url: Constants.baseURL,
                                       statusCode: 200,
                                       httpVersion: nil,
                                       headerFields: nil)
        session.response = response

        let euros = CurrencyDetailServiceResponse(code: "EUR", symbol: "€", value: 9237.9529)
        let dollars = CurrencyDetailServiceResponse(code: "USD", symbol: "$", value: 7490.0605)
        let pounds = CurrencyDetailServiceResponse(code: "GBP", symbol: "£", value: 9426.5109)
        let currencies = CurrenciesServiceResponse(euros: euros, dollars: dollars, pounds: pounds)
        let currentPrice = CurrentPriceServiceResponse(currencies: currencies)
        let encoder = JSONEncoder()
        let data = try? encoder.encode(currentPrice)

        session.data = data
        webService = WebService(session: session)
        repository = CurrentPriceRepository(webService: webService)

        // When
        let repositoryExpectation = expectation(description: "CurrentPriceRepositoryExpectationSuccess")
        repository.currentPrice { result in

            switch result {
            case .success(let soaCurrentPrice):
                // Then
                XCTAssertEqual(soaCurrentPrice.currencies.euros.value, 9237.9529)
                XCTAssertEqual(soaCurrentPrice.currencies.euros.code, "EUR")
                XCTAssertEqual(soaCurrentPrice.currencies.euros.symbol, "€")
                XCTAssertEqual(soaCurrentPrice.currencies.dollars.value, 7490.0605)
                XCTAssertEqual(soaCurrentPrice.currencies.dollars.code, "USD")
                XCTAssertEqual(soaCurrentPrice.currencies.dollars.symbol, "$")
                XCTAssertEqual(soaCurrentPrice.currencies.pounds.value, 9426.5109)
                XCTAssertEqual(soaCurrentPrice.currencies.pounds.code, "GBP")
                XCTAssertEqual(soaCurrentPrice.currencies.pounds.symbol, "£")
                repositoryExpectation.fulfill()
            case .failure:
                XCTFail("CurrentPriceRepositoryTests failed when expected to succeed")
            }
        }

        // Then
        waitForExpectations(timeout: Constants.timeout) { error in

            if let error = error {
                XCTFail("CurrentPriceRepositoryTests error: \(error)")
            }
        }
    }

    func test_CurrentPrice_failure() {

        // Given
        session = URLSessionMock()
        let error = NSError(domain: "domain", code: 000, userInfo: nil)
        session.error = error
        webService = WebService(session: session)
        repository = CurrentPriceRepository(webService: webService)

        // When
        let repositoryExpectation = expectation(description: "CurrentPriceRepositoryExpectationFailure")
        repository.currentPrice { result in

            switch result {
            case .success:
                XCTFail("CurrentPriceRepositoryTests succeeded when expected to fail")
            case .failure(let error):
                XCTAssertNotNil(error)
                repositoryExpectation.fulfill()
            }
        }

        // Then
        waitForExpectations(timeout: Constants.timeout) { error in

            if let error = error {
                XCTFail("CurrentPriceRepositoryTests error: \(error)")
            }
        }
    }
}
