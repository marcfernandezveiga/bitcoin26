//
//  HistoricalPricePresenterTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

// MARK: - HistoricalPriceViewMock
class HistoricalPriceViewMock: HistoricalPriceView {

    private(set) var startLoadingCalled = false
    private(set) var stopLoadingCalled = false
    private(set) var updatePricesCalled = false
    private(set) var showErrorCalled = false

    var title: String?

    func setLoading(_ loading: Bool) {
        if loading { startLoadingCalled = true } else { stopLoadingCalled = true }
    }

    func update(with prices: [Price]) {
        updatePricesCalled = true
    }

    func showError(_ error: ServiceError) {
        showErrorCalled = true
    }
}

// MARK: - HistoricalPriceRepositoryMock
class HistoricalPriceRepositoryMock: HistoricalPriceRepositoryProtocol {

    var forceError = false

    func historicalPrice(completion completed: @escaping (Result<HistoricalPrice, ServiceError>) -> Void) {

        let calendar = Calendar.current
        var components = DateComponents()
        components.year = 2019
        components.month = 10
        components.day = 14
        let date = calendar.date(from: components)!
        let price = Price(value: 25434.124, date: date)
        let historical = HistoricalPrice(prices: [price])
        let serviceError = ServiceError.unexpected

        _ = forceError ? completed(.failure(serviceError)) : completed(.success(historical))
    }
}

// MARK: - EmbededNavigatorMock
class EmbededNavigatorMock: EmbededNavigator {

    private(set) var embedViewControllerCalled = false

    func embedViewController(in containerView: UIView,
                             from viewController: UIViewController) {
        embedViewControllerCalled = true
    }
}

// MARK: - DetailNavigatorMock
class DetailNavigatorMock: DetailNavigator {

    private(set) var showDetailCalled = false

    func showDetail<T>(item: T) {
        showDetailCalled = true
    }
}

// MARK: - HistoricalPricePresenterTests
class HistoricalPricePresenterTests: XCTestCase {

    // MARK: - Variables
    private var repository: HistoricalPriceRepositoryMock!
    private var view: HistoricalPriceViewMock!
    private var embededNavigator: EmbededNavigatorMock!
    private var detailNavigator: DetailNavigatorMock!
    private var presenter: HistoricalPricePresenter!

    // MARK: - Tests
    func test_loadView() {

        // Given
        repository = HistoricalPriceRepositoryMock()
        repository.forceError = false
        view = HistoricalPriceViewMock()
        embededNavigator = EmbededNavigatorMock()
        detailNavigator = DetailNavigatorMock()
        presenter = HistoricalPricePresenter(embededNavigator: embededNavigator,
                                             detailNavigator: detailNavigator,
                                             historicalPriceRepository: repository)
        presenter.view = view

        // When
        presenter.loadView()

        // Then
        XCTAssertEqual(view.title, "Bitcoin26")
        XCTAssertTrue(view.startLoadingCalled)
        XCTAssertTrue(view.stopLoadingCalled)
        XCTAssertTrue(view.updatePricesCalled)
    }

    func test_embedCurrentPrice() {

        // Given
        repository = HistoricalPriceRepositoryMock()
        repository.forceError = true
        view = HistoricalPriceViewMock()
        embededNavigator = EmbededNavigatorMock()
        detailNavigator = DetailNavigatorMock()
        presenter = HistoricalPricePresenter(embededNavigator: embededNavigator,
                                             detailNavigator: detailNavigator,
                                             historicalPriceRepository: repository)
        presenter.view = view
        let containerView = UIView()
        let viewController = UIViewController()

        // When
        presenter.embedCurrentPrice(in: containerView, from: viewController)

        // Then
        XCTAssertTrue(embededNavigator.embedViewControllerCalled)
    }

    func test_fetchHistorical_fail() {

        // Given
        repository = HistoricalPriceRepositoryMock()
        repository.forceError = true
        view = HistoricalPriceViewMock()
        embededNavigator = EmbededNavigatorMock()
        detailNavigator = DetailNavigatorMock()
        presenter = HistoricalPricePresenter(embededNavigator: embededNavigator,
                                             detailNavigator: detailNavigator,
                                             historicalPriceRepository: repository)
        presenter.view = view

        // When
        presenter.fetchHistoricalPrice()

        // Then
        XCTAssertTrue(view.startLoadingCalled)
        XCTAssertTrue(view.stopLoadingCalled)
        XCTAssertTrue(view.showErrorCalled)
    }

    func test_didSelect() {

        // Given
        repository = HistoricalPriceRepositoryMock()
        repository.forceError = true
        view = HistoricalPriceViewMock()
        embededNavigator = EmbededNavigatorMock()
        detailNavigator = DetailNavigatorMock()
        presenter = HistoricalPricePresenter(embededNavigator: embededNavigator,
                                             detailNavigator: detailNavigator,
                                             historicalPriceRepository: repository)
        presenter.view = view
        let price = Price(value: 0, date: Date())

        // When
        presenter.didSelect(price: price)

        // Then
        XCTAssertTrue(detailNavigator.showDetailCalled)
    }
}
