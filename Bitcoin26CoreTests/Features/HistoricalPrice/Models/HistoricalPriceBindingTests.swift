//
//  HistoricalPriceBindingTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class HistoricalPriceBindingTests: XCTestCase {

    func test_binding() {

        let soaPrices = ["2019-09-07": 9426.5109, "2019-10-07": 7490.0605, "2019-09-20": 9237.9529]
        let soaHistoricalPrice = HistoricalPriceServiceResponse(prices: soaPrices)

        let historicalPrice = HistoricalPriceBinding.bind(soaHistoricalPrice)

        XCTAssertEqual(historicalPrice.prices.count, 3)

        var price: Price
        price = historicalPrice.prices[0]
        XCTAssertEqual(price.value, 7490.0605)
        price = historicalPrice.prices[1]
        XCTAssertEqual(price.value, 9237.9529)
        price = historicalPrice.prices[2]
        XCTAssertEqual(price.value, 9426.5109)
    }
}
