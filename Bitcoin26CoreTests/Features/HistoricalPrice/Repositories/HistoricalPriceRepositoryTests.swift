//
//  HistoricalRepositoryTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class HistoricalPriceRepositoryTests: XCTestCase {

    // MARK: - Constants
    private enum Constants {
        static let baseURL: URL = URL(string: "www.test.com")!
        static let timeout: TimeInterval = 5
    }

    // MARK: - Variables
    private var session: URLSessionMock!
    private var webService: WebService!
    private var repository: HistoricalPriceRepository!

    func test_historicalPrice_success() {

        // Given
        session = URLSessionMock()
        let response = HTTPURLResponse(url: Constants.baseURL,
                                       statusCode: 200,
                                       httpVersion: nil,
                                       headerFields: nil)
        session.response = response

        let prices = ["2019-09-07": 9426.5109, "2019-10-07": 7490.0605, "2019-09-20": 9237.9529]
        let historicalPrice = HistoricalPriceServiceResponse(prices: prices)
        let encoder = JSONEncoder()
        let data = try? encoder.encode(historicalPrice)

        session.data = data
        webService = WebService(session: session)
        repository = HistoricalPriceRepository(webService: webService)

        // When
        let repositoryExpectation = expectation(description: "HistoricalPriceRepositoryExpectationSuccess")
        repository.historicalPrice { result in

            switch result {
            case .success(let soaHistoricalPrice):
                // Then
                XCTAssertEqual(soaHistoricalPrice.prices.count, 3)
                repositoryExpectation.fulfill()
            case .failure:
                XCTFail("HistoricalPriceRepositoryTests failed when expected to succeed")
            }
        }

        // Then
        waitForExpectations(timeout: Constants.timeout) { error in

            if let error = error {
                XCTFail("HistoricalPriceRepositoryTests error: \(error)")
            }
        }
    }

    func test_historicalPrice_failure() {

        // Given
        session = URLSessionMock()
        let error = NSError(domain: "domain", code: 000, userInfo: nil)
        session.error = error
        webService = WebService(session: session)
        repository = HistoricalPriceRepository(webService: webService)

        // When
        let repositoryExpectation = expectation(description: "HistoricalPriceRepositoryExpectationFailure")
        repository.historicalPrice { result in

            switch result {
            case .success:
                XCTFail("HistoricalPriceRepositoryTests succeeded when expected to fail")
            case .failure(let error):
                XCTAssertNotNil(error)
                repositoryExpectation.fulfill()
            }
        }

        // Then
        waitForExpectations(timeout: Constants.timeout) { error in

            if let error = error {
                XCTFail("HistoricalPriceRepositoryTests error: \(error)")
            }
        }
    }
}
