//
//  HistoricalPriceCellTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class HistoricalPriceCellTests: XCTestCase {

    // MARK: - Variables
    var cell: HistoricalPriceCell!
    var date: Date {
        let calendar = Calendar.current
        var components = DateComponents()
        components.year = 2019
        components.month = 10
        components.day = 14
        return calendar.date(from: components)!
    }

    override func setUp() {
        super.setUp()

        cell = HistoricalPriceCell.instantiate()
    }

    // MARK: - Tests
    func test_prepareForReuse() {

        cell.prepareForReuse()

        XCTAssertNil(cell.dateLabel.text)
        XCTAssertNil(cell.priceLabel.text)
        XCTAssertNil(cell.stateImageView.image)
    }

    func test_bind_increased() {

        var price = Price(value: 25434.124, date: date)
        price.state = .increased
        cell.bind(with: price)

        XCTAssertEqual(cell.dateLabel.text, "14/10/2019")
        XCTAssertEqual(cell.priceLabel.text, "25.434,12\u{00a0}€")
        let image = UIImage(named: "increased",
                            in: Bundle.core,
                            compatibleWith: nil)
        XCTAssertNotNil(cell.stateImageView.image)
        XCTAssertEqual(cell.stateImageView.image, image)
    }

    func test_bind_decreased() {

        var price = Price(value: 25434.124, date: date)
        price.state = .decreased
        cell.bind(with: price)

        XCTAssertEqual(cell.dateLabel.text, "14/10/2019")
        XCTAssertEqual(cell.priceLabel.text, "25.434,12\u{00a0}€")
        let image = UIImage(named: "decreased",
                            in: Bundle.core,
                            compatibleWith: nil)
        XCTAssertNotNil(cell.stateImageView.image)
        XCTAssertEqual(cell.stateImageView.image, image)
    }

    func test_bind_maintained() {

        var price = Price(value: 25434.124, date: date)
        price.state = .maintained
        cell.bind(with: price)

        XCTAssertEqual(cell.dateLabel.text, "14/10/2019")
        XCTAssertEqual(cell.priceLabel.text, "25.434,12\u{00a0}€")
        let image = UIImage(named: "maintained",
                            in: Bundle.core,
                            compatibleWith: nil)
        XCTAssertNotNil(cell.stateImageView.image)
        XCTAssertEqual(cell.stateImageView.image, image)
    }

    func test_bind_nil() {

        let price = Price(value: 25434.124, date: date)
        cell.bind(with: price)

        XCTAssertEqual(cell.dateLabel.text, "14/10/2019")
        XCTAssertEqual(cell.priceLabel.text, "25.434,12\u{00a0}€")
        XCTAssertNil(cell.stateImageView.image)
    }
}
