//
//  EndpointTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class EndpointTests: XCTestCase {

    // MARK: - Constants
    private enum Constants {
        static let baseURL: URL = URL(string: "www.test.com")!
        static let currency: String = "EUR"
    }

    // MARK: - Tests
    func test_historical() {

        let endpoint: Endpoint = .historical(currencyCode: Constants.currency)
        let request = endpoint.request(with: Constants.baseURL, adding: [:])
        XCTAssertNotNil(request)
        XCTAssertEqual(request.url?.path, "www.test.com/historical/close.json")
        XCTAssertEqual(request.httpMethod, "GET")

        if let absoluteString = request.url?.absoluteString,
            let components = URLComponents(string: absoluteString) {

            let parameter = components.queryItems?.first { $0.name == "currency" }?.value
            XCTAssertEqual(parameter, Constants.currency)
        }
    }

    func test_current() {

        let endpoint: Endpoint = .current
        let request = endpoint.request(with: Constants.baseURL, adding: [:])
        XCTAssertNotNil(request)
        XCTAssertEqual(request.url?.path, "www.test.com/currentprice.json")
        XCTAssertEqual(request.httpMethod, "GET")
    }
}
