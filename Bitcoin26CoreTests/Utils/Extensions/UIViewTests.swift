//
//  UIViewTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class UIViewTests: XCTestCase {

    // MARK: - Variables
    var view: UIView!

    // MARK: - Lifecycle Methods
    override func setUp() {
        super.setUp()

        view = UIView()
    }

    // MARK: - Tests
    func test_cornerRadius() {

        view.cornerRadius = 20
        XCTAssertEqual(view.layer.cornerRadius, 20)
        XCTAssertEqual(view.cornerRadius, 20)
    }

    func test_shadowColor() {

        view.shadowColor = .red
        XCTAssertEqual(view.layer.shadowColor, UIColor.red.cgColor)
        XCTAssertEqual(view.shadowColor, .red)
    }

    func test_fitSubview() {

        let frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        let viewA = UIView(frame: frame)
        let viewB = UIView()
        viewA.fitSubview(viewB)
        XCTAssertEqual(viewB.superview, viewA)
    }
}
