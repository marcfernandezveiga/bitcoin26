//
//  UITableViewTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class UITableViewTests: XCTestCase {

    // MARK: - Variables
    var tableView: UITableView!

    // MARK: - Lifecycle Methods
    override func setUp() {
        super.setUp()

        tableView = UITableView()
    }

    // MARK: - Tests
    func test_dequeueReusableCell() {

        tableView.register(TestCell.self)
        let registeredCell = tableView.dequeueReusableCell(TestCell.self)
        XCTAssertNotNil(registeredCell)
    }

    func test_dequeueReusableCell_indexPath() {

        tableView.register(TestCell.self)
        let registeredCell = tableView.dequeueReusableCell(TestCell.self, for: IndexPath(row: 0, section: 0))
        XCTAssertNotNil(registeredCell)
    }
}
