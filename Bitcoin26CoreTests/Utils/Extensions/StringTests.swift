//
//  StringTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 11/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class StringTests: XCTestCase {

    // MARK: - Tests
    func test_localized_01() {

        XCTAssertEqual("missing_key".localized, "**missing_key**")
    }

    func test_localizedText_02() {

        XCTAssertEqual("app_missing_key".localized, "**app_missing_key**")
    }

    func test_localizedText_03() {

        XCTAssertEqual("core_historicalPrice_title".localized, "Bitcoin26")
    }
}
