//
//  UILabelTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 11/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class UILabelTests: XCTestCase {

    // MARK: - Variables
    var label: UILabel!

    // MARK: - Lifecycle Methods
    override func setUp() {
        super.setUp()

        label = UILabel()
    }

    // MARK: - Tests
    func test_localizedText_01() {

        XCTAssertEqual(label.localizedText, "")
        label.localizedText = "core_historicalPrice_title"
        XCTAssertEqual(label.text, "Bitcoin26")
        XCTAssertEqual(label.localizedText, "Bitcoin26")
    }
}
