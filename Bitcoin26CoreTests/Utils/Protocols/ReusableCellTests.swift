//
//  ReusableCellTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest

class ReusableCellTests: XCTestCase {

    func testReuseIdentifier() {

        let reuseIdentifier = TestCell.reuseIdentifier
        XCTAssertEqual(reuseIdentifier, "TestCell")
    }
}
