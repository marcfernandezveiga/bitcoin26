//
//  NibLoadableViewTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest

class NibLoadableViewTests: XCTestCase {

    func testNibName() {

        let testCellNibName = TestCell.nibName
        XCTAssertEqual(testCellNibName, "TestCell")
    }

    func testInstantiate() {

        let testCell = TestCell.instantiate()
        XCTAssertNotNil(testCell)
    }
}
