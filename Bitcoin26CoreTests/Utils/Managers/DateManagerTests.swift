//
//  DateManagerTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class DateManagerTests: XCTestCase {

    // MARK: - Variables
    private var date: Date!

    // MARK: - Lifecycle Methods
    override func setUp() {
        super.setUp()

        let calendar = Calendar.current
        var components = DateComponents()
        components.year = 2019
        components.month = 10
        components.day = 14
        date = calendar.date(from: components)
    }

    // MARK: - Tests
    func test_date_format_01() {

        let date = DateManager.date(from: "14/10/2019", format: .ddMMyyyy)
        XCTAssertEqual(date, self.date)
    }

    func test_date_format_02() {

        let date = DateManager.date(from: "10/14/2019", format: .MMddyyyy)
        XCTAssertEqual(date, self.date)
    }

    func test_date_format_03() {

        let date = DateManager.date(from: "2019-10-14", format: .yyyy_MM_dd)
        XCTAssertEqual(date, self.date)
    }

    func test_string_format_01() {

        let string = DateManager.string(from: date, format: .ddMMyyyy)
        XCTAssertEqual(string, "14/10/2019")
    }

    func test_string_format_02() {

        let string = DateManager.string(from: date, format: .MMddyyyy)
        XCTAssertEqual(string, "10/14/2019")
    }

    func test_string_format_03() {

        let string = DateManager.string(from: date, format: .yyyy_MM_dd)
        XCTAssertEqual(string, "2019-10-14")
    }
}
