//
//  PersistentDataManagerTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 10/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class PersistentDataManagerTests: XCTestCase {

    func test_save_nil() {

        PersistentDataManager.eurosToDollarsFactor.value = nil
        let factor = UserDefaults.standard.value(forKey: "eurosToDollarsFactor")
        XCTAssertNil(factor)
        let storedValue = PersistentDataManager.eurosToDollarsFactor.value
        XCTAssertNil(storedValue)
    }

    func test_save() {

        PersistentDataManager.eurosToDollarsFactor.value = 100
        let factor = UserDefaults.standard.value(forKey: "eurosToDollarsFactor")
        XCTAssertNotNil(factor)
        XCTAssert(factor is Double)
        let storedValue = PersistentDataManager.eurosToDollarsFactor.value
        XCTAssertNotNil(storedValue)
        XCTAssertEqual(storedValue, 100)
    }
}
