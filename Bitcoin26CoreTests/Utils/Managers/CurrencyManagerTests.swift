//
//  CurrencyManagerTests.swift
//  Bitcoin26CoreTests
//
//  Created by Marc Fernandez Veiga on 08/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest
@testable import Bitcoin26Core

class CurrencyManagerTests: XCTestCase {

    // MARK: - Variables
    private var formatted: String?

    // MARK: - Tests    
    func test_format_euro() {

        formatted = CurrencyManager.format(value: 52342.6543, currency: .euro)
        XCTAssertEqual(formatted, "52.342,65\u{00a0}€")
        formatted = CurrencyManager.format(value: 2342.6543, currency: .euro)
        XCTAssertEqual(formatted, "2342,65\u{00a0}€")
        formatted = CurrencyManager.format(value: 342.6543, currency: .euro)
        XCTAssertEqual(formatted, "342,65\u{00a0}€")
    }

    func test_format_dollar() {

        formatted = CurrencyManager.format(value: 52342.6543, currency: .dollar)
        XCTAssertEqual(formatted, "$52,342.65")
        formatted = CurrencyManager.format(value: 2342.6543, currency: .dollar)
        XCTAssertEqual(formatted, "$2,342.65")
        formatted = CurrencyManager.format(value: 342.6543, currency: .dollar)
        XCTAssertEqual(formatted, "$342.65")
    }

    func test_format_pound() {

        formatted = CurrencyManager.format(value: 52342.6543, currency: .pound)
        XCTAssertEqual(formatted, "£52,342.65")
        formatted = CurrencyManager.format(value: 2342.6543, currency: .pound)
        XCTAssertEqual(formatted, "£2,342.65")
        formatted = CurrencyManager.format(value: 342.6543, currency: .pound)
        XCTAssertEqual(formatted, "£342.65")
    }

    func test_convertToDollars_nil() {

        PersistentDataManager.eurosToDollarsFactor.value = nil
        let euros: Double = 100
        let dollars = CurrencyManager.convertToDollars(euros: euros)
        XCTAssertNil(dollars)
    }

    func test_convertToDollars() {

        PersistentDataManager.eurosToDollarsFactor.value = 2
        let euros: Double = 100
        let dollars = CurrencyManager.convertToDollars(euros: euros)
        XCTAssertEqual(dollars, 200)
    }

    func test_convertToPounds_nil() {

        PersistentDataManager.eurosToPoundsFactor.value = nil
        let euros: Double = 100
        let pounds = CurrencyManager.convertToPounds(euros: euros)
        XCTAssertNil(pounds)
    }

    func test_convertToPunds() {

        PersistentDataManager.eurosToPoundsFactor.value = 2
        let euros: Double = 100
        let pounds = CurrencyManager.convertToPounds(euros: euros)
        XCTAssertEqual(pounds, 200)
    }
}
