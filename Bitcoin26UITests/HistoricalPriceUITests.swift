//
//  HistoricalPriceUITests.swift
//  Bitcoin26UITests
//
//  Created by Marc Fernandez Veiga on 11/10/2019.
//  Copyright © 2019 marcfernandezveiga. All rights reserved.
//

import XCTest

class HistoricalPriceUITests: XCTestCase {

    // MARK: - Variables
    private var isDisplayingHistoricalPrice: Bool {
        return app.otherElements["HistoricalPriceView"].waitForExistence(timeout: self.timeout)
    }
    private var isDisplayingCurrentPrice: Bool {
        return app.otherElements["CurrentPriceView"].waitForExistence(timeout: self.timeout)
    }
    private var isDisplayingDetail: Bool {
        return app.otherElements["DetailView"].waitForExistence(timeout: self.timeout)
    }

    var app: XCUIApplication!
    var timeout: Double = 5

    // MARK: - Lifecycle Methods
    override func setUp() {
        app = XCUIApplication()
        continueAfterFailure = false
    }

    // MARK: - Private Methods
    private func goToHistoricalPrice() {

        app.launch()

        XCTAssertTrue(isDisplayingHistoricalPrice)
        XCTAssertTrue(isDisplayingCurrentPrice)
    }

    // MARK: - Tests
    func test_detail() {

        goToHistoricalPrice()
        let tableView = app.tables["HistoricalPriceTableView"]
        let cell = tableView.cells["HistoricalPriceCell_0"]
        cell.tap()
        XCTAssertTrue(isDisplayingDetail)
    }
}
